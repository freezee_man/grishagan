from requests_html import HTMLSession
from pandas.io.json import json_normalize

session = HTMLSession()

metrika_headers = {
    'GET': '/management/v1/counters HTTP/1.1',
    'Host': 'api-metrika.yandex.net',
    'Authorization': 'OAuth AgAAAAAEsZp6AAZC-_6p-0T04E0Hkcn4XePsiTM',
    'Content-Type': 'application/x-yametrika+json'
}

counetr_id = "51644453"

metrika_url = 'https://api-metrika.yandex.net/stat/v1/data?preset=sources_search_phrases&id=51644453&date1=2018-10-30&date2=today'
json_content_response = session.get(metrika_url, headers=metrika_headers).json()
data = json_normalize(json_content_response['data'], 'dimensions')
f = open('output.txt', 'w')
f.write(json_content_response)
f.close()
print(json_content_response)
print(data['name'])
