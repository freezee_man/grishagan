import json
from urllib.parse import urlencode

import pandas as pd
import pymorphy2
from pandas.io.json import json_normalize
from requests_html import HTMLSession

from utils import split_data

morph = pymorphy2.MorphAnalyzer()


def lemmatize(text):
    words = text.split()  # разбиваем текст на слова
    res = list()
    for word in words:
        p = morph.parse(word)[0]
        res.append(p.normal_form)

    res1 = " ".join(res)
    return res1


counter_id = 390119
start_date = '2017-10-30'
columns_d = ['startURLPath', 'lastSearchPhrase', 'lastSearchEngine']
columns_d_prefix = "ym:s:"
columns_m = ['visits']
goals = [16220045, 16220095, 59513986, 50918953, 57387445, 57387460]
columns_m_prefix = "ym:s:"
limit = 1000

columns_goals = [f'goal{goal}reaches' for goal in goals]


def load_metrika_data():
    session = HTMLSession()

    # oauth_id_web_comp = "AgAAAAAYBtdTAAZEzHZuGGbAvEVrpBkhJcwc0qc"
    # oauth_id_dima2010 = "AgAAAAAEsZp6AAZC-_6p-0T04E0Hkcn4XePsiTM"
    metrika_headers = {
        "GET": "/management/v1/counters HTTP/1.1",
        "Host": "api-metrika.yandex.net",
        "Authorization": "OAuth AgAAAAAYBtdTAAZEzHZuGGbAvEVrpBkhJcwc0qc",
        "Content-Type": "application/x-yametrika+json"
    }

    dimensions = ','.join(map(lambda d: f"{columns_d_prefix}{d}", columns_d))
    metrics = ','.join(map(lambda m: f"{columns_m_prefix}{m}", columns_m + columns_goals))
    query_params = {
        'preset': 'sources_search_phrases',
        'id': counter_id,
        'date1': start_date,
        'date2': 'yesterday',
        'metrics': metrics,
        'dimensions': dimensions,
        'filters': "ym:s:lastSearchEngine=='yandex_search'",
        'limit': limit
    }
    last_url = f"https://api-metrika.yandex.net/stat/v1/data?{urlencode(query_params)}"
    json_content_response = session.get(last_url, headers=metrika_headers)
    print("Файл Metrika скачан")
    json_content = json.loads(json_content_response.content)

    with open("data/raw_output.json", "wb") as f:
        f.write(json.dumps(json_content, indent=2, ensure_ascii=False).encode('utf-8'))
    print("Файл Metrika сохранен как raw_json")

    try:
        data = json_content['data']
    except KeyError:
        raise Exception("Ошибка при запросе Metrika")
    data = list(filter(lambda elem: bool(elem['dimensions'][1]['name']), data))
    data = list(map(lambda elem: split_data(elem, columns_d, columns_m + columns_goals), data))

    with open("data/output.json", "wb") as f:
        f.write(json.dumps(data, indent=2, ensure_ascii=False).encode('utf-8'))
    print("Файл Metrika сохранен как json")

    return data


def metrika_api():
    data = load_metrika_data()

    for key, row in enumerate(data):
        sum_goals = sum([row[goal] for goal in columns_goals])
        new_row = {
            'Keyword_raw': row['lastSearchPhrase'],
            'Keyword': lemmatize(row['lastSearchPhrase']),
            'URL': row['startURLPath'],
            'Click': row['visits'],
            'KPI': sum_goals
        }
        data[key] = new_row

    flat_table = json_normalize(data)
    flat_table.to_excel('data/output.xlsx', index=False, columns=['Keyword_raw', 'Keyword', 'URL', 'Click', 'KPI'])
    print("Файл Metrika сохранен как xlsx")


#  функция для обучения модели
def weighted_average_conversion_words(report):
    #в полученной таблице выкидываются все неправильные или отсутствующие значения и перестраивается индекс 1,2,3..
    report = report.dropna().reset_index(drop=True)
    temp = []

    # пройтись по каждой строке чтобы преобразовать массив с длинными keyword в массив с одинарным keyword
    for i in range(len(report)):
        print("generating temp for abc " + str((float(i) / len(report)) * 100) + " %")
        if report.loc[i, "Keyword"] is not None and type(report.loc[i, "Keyword"]) == str:
            for j in report.loc[i, "Keyword"].split(" "):
                temp.append([j, report.loc[i, "URL"], report.loc[i, "Click"], report.loc[i, "KPI"]])

    # из полученного списка с данными создать таблицу DataFrame
    words = pd.DataFrame(temp, columns=["Word", "URL", "Click", "KPI"])
    words.loc[words.Click > 100, "Click"] = 100

    # считаем метрики по группам
    words["WA_Word"] = words["KPI"] * (words["Click"] / words.groupby(["Word", "URL"])["Click"].transform(sum))

    words["WA_Word"] = words.groupby(["Word", "URL"])["WA_Word"].transform(sum)
    words["WA_Click"] = words.groupby(["Word", "URL"])["Click"].transform(sum)
    words.loc[words.WA_Click > 100, "WA_Click"] = 100

    words = words[["Word", "URL", "WA_Word", "WA_Click"]].drop_duplicates().reset_index(drop=True)

    return words


# функция для прогнозирования конверсии на основании модели
def predict_conversion_words(words, report):
    temp = []
    for i in range(len(report)):
        print("generating temp for prediction " + str((float(i) / len(report)) * 100) + " %")
        if report.loc[i, "Keyword"] is not None and type(report.loc[i, "Keyword"]) == str:
            for j in report.loc[i, "Keyword"].split(" "):
                temp.append([report.loc[i, "Keyword"], report.loc[i, "URL"], j])

    # грузим новые данные в таблицу
    keywords = pd.DataFrame(temp, columns=["Keyword", "URL", "Word"])

    keywords.loc[:, "KPI"] = 0
    keywords.loc[:, "Click"] = 0
    keywords.loc[:, "n_words"] = 0

    # для каждой строки находим соответствие WA_Word и WA_Click и подставляем в табличку
    for i in range(len(keywords)):
        print("prediction " + str((float(i) / len(keywords)) * 100) + " %")
        # print(words["Word"] + "; " + keywords.loc[i, "Word"] + "; " + words["URL"] + "; " + keywords.loc[i, "URL"])
        if ((words["Word"] == keywords.loc[i, "Word"]) & (words["URL"] == keywords.loc[i, "URL"])).any():

            #             print keywords.loc[i, "Word"] + " == " \
            #                 + words.loc[(words.Word == keywords.loc[i, "Word"]) & (words.URL == keywords.loc[i, "URL"]), "Word"]
            #             print words.loc[(words.Word == keywords.loc[i, "Word"]) & (words.URL == keywords.loc[i, "URL"]), "URL"] \
            #                 + " == "
            #             print keywords.loc[i, "URL"]

            keywords.loc[i, "KPI"] = float(
                words.loc[(words.Word == keywords.loc[i, "Word"]) & (words.URL == keywords.loc[i, "URL"]), "WA_Word"])
            keywords.loc[i, "Click"] = float(
                words.loc[(words.Word == keywords.loc[i, "Word"]) & (words.URL == keywords.loc[i, "URL"]), "WA_Click"])
        else:
            keywords.loc[i, "KPI"] = 0
            keywords.loc[i, "Click"] = 0
            keywords.loc[i, "n_words"] += 1

    # всякие шаманские группировки данных для удаления дубликатов
    keywords["WA_Keyword"] = keywords["KPI"] * (
                keywords["Click"] / keywords.groupby(["Keyword", "URL"])["Click"].transform(sum))

    keywords["WA_Keyword"] = keywords.groupby(["Keyword", "URL"])["WA_Keyword"].transform(sum)
    keywords["Click"] = keywords.groupby(["Keyword", "URL"])["Click"].transform(sum)
    keywords["n_words"] = keywords.groupby(["Keyword", "URL"])["n_words"].transform(sum)

    result = keywords[["Keyword", "URL", "WA_Keyword", "Click", "n_words"]].drop_duplicates().reset_index(drop=True)

    return pd.merge(report, result, how="left", on=["Keyword", "URL"])


# training_set - Excel файл для обучения модели с колонками Keyword_raw, Keyword, URL, Click, KPI
# data - Excel файл для прогнозирования с колонками ID, Keyword, URL

# исполняющая функция
def predict_conversion(training_set, data):
    # открываем обучающую выборку
    training_set = pd.read_excel(training_set)

    # расчитываем конверсию
    training_set.KPI = training_set.KPI / training_set.Click

    # выодим на экран таблицу обучающей выборки
    print(training_set)

    # вызываем функцию для обучения модели
    words = weighted_average_conversion_words(training_set)

    # выодим на экран модель
    print(words)

    # сохраняем словарь в отдельный файл на будущее
    words.to_excel("data/words_wa.xlsx")

    # открываем словарь
    words = pd.read_excel("data/words_wa.xlsx")

    # открываем данные для прогноза
    data = pd.read_excel(data)

    # выводим на экран таблицу с данными для прогноза
    print(data)

    # выполняем функцию прогнозирования на основании модели
    report = predict_conversion_words(words, data)

    # выводим рузультаты прогноза
    print(report)

    # сохраняем таблицу с прогнозами в отдельном файле
    report.to_excel("data/result.xlsx")


if __name__ == '__main__':
    metrika_api()
    predict_conversion("data/output.xlsx", "data/output.xlsx")
