def split_data(elem, columns_d, columns_m):
    dimensions_def = {v: k for k, v in enumerate(columns_d)}
    metrics_def = {v: k for k, v in enumerate(columns_m)}
    d = elem['dimensions']
    m = elem['metrics']

    def get_dim(name):
        return d[dimensions_def[name]]

    config = {
        'startURLPath': get_dim('startURLPath')['name'],
        'lastSearchPhrase': get_dim('lastSearchPhrase')['name'],
    }
    elem = {
        **{column: config[column] for column in columns_d if column in config},
        **{column: m[metrics_def[column]] for column in columns_m}
    }
    return elem
